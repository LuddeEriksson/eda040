package uppgift;

import lift.LiftView;

public class Monitor {
	private int here;
	private int there;
	private int[] waitEntry;
	private int[] waitExit;
	private int load;
	private LiftView lv;

	public Monitor(LiftView lv) {
		this.lv = lv;
		waitEntry = new int[7];
		waitExit = new int[7];
		// there = 1;
	}

	synchronized public void person(int start, int target) {
		// getting in line
		waitEntry[start]++;
		notifyAll();
		lv.drawLevel(start, waitEntry[start]);
		while (here != start || load == 4 || here != there) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
		// shouldnt get on lift if lift is moving, i.e between floors but values
		// same
		// getting on the elevator
		load++;
		waitExit[target]++;
		waitEntry[start]--;
		lv.drawLift(here, load);
		lv.drawLevel(here, waitEntry[start]);
		notifyAll();

		while (here != target) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
		// leaving the elevator
		load--;
		waitExit[target]--;
		lv.drawLift(here, load);
		notifyAll(); // notifies that people has left the elvator
	}

	synchronized public void updateLevels(int here, int there) {
		while (load == 0 && !Waiting()) {
			try {
				wait();
			} catch (InterruptedException e) {
				System.err.println("Avslutar snyggt");
				System.exit(1);
			}
		}

		this.here = here;
		notifyAll();
		while (cantMove()) {
			try {
				wait();
			} catch (InterruptedException e) {
				System.err.println("Avslutar snyggt");
				System.exit(1);
			}
		}
		this.there = there;
	}

	private boolean Waiting() {
		boolean returned = false;
		for (int i = 0; i < 7; i++) {
			if (waitEntry[i] > 0) {
				returned = true;
			}
		}
		return returned;
	}

	public boolean cantMove() {
		//
		return (waitEntry[here] != 0 && load != 4) || waitExit[here] != 0;

	}

}
