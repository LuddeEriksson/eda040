package uppgift;

import java.util.ArrayList;
import java.util.Collections;

public class Person extends Thread {
	private int startFloor;
	private int endFloor;
	private Monitor monitor;

	public Person(Monitor monitor) {
		this.monitor = monitor;
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(1000*(( int )( Math.random ()*46.0)));
			} catch (InterruptedException e) {
			}
			loop();
		}
	}

	private void loop() {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < 7; i++) {
			list.add(new Integer(i));
		}
		Collections.shuffle(list);
		startFloor = list.get(0);
		endFloor = list.get(1);
		monitor.person(startFloor, endFloor);
		

	}

}
