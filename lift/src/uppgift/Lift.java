package uppgift;

import lift.LiftView;

public class Lift extends Thread {
	private LiftView lv;
	private Monitor monitor;
	private boolean test;

	public Lift(LiftView lv, Monitor monitor) {
		this.lv = lv;
		this.monitor = monitor;
	}

	public void run() {
		while (true) {

			for (int i = 0; i < 6; i++) {
				monitor.updateLevels(i, i + 1);
				lv.moveLift(i, i + 1);

			}
			for (int i = 6; i > 0; i--) {
				monitor.updateLevels(i, i - 1);
				lv.moveLift(i, i - 1);

			}
		}
	}

}
