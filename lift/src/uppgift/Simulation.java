package uppgift;

import java.util.ArrayList;

import lift.LiftView;

public class Simulation {

	public static void main(String[] args) {
		 LiftView lv = new LiftView();
		 Monitor monitor = new Monitor(lv);
		 ArrayList<Person> persons = new ArrayList();
		 for(int i = 0; i < 20; i++){
			 persons.add(new Person(monitor));
		 }
		 
		 Lift l = new Lift(lv, monitor);
		 l.start();	
		 for(Person p: persons){
			 p.start();
		 }
		// p.start();
		 

	}
}
