package todo;

import done.ClockInput;
import se.lth.cs.realtime.semaphore.CountingSem;
import se.lth.cs.realtime.semaphore.MutexSem;
import se.lth.cs.realtime.semaphore.Semaphore;

public class SetThread extends Thread {

	private SharedData sd;
	private ClockInput i;
	private Semaphore sem;
	private int prevValue;

	public SetThread(SharedData sd, ClockInput i) {
		this.i = i;
		this.sd = sd;
		sem = i.getSemaphoreInstance();

	}

	public void run() {
		while (true) {
			sem.take();
			switch (i.getChoice()) {
			case ClockInput.SET_TIME:
				prevValue = ClockInput.SET_TIME;
				break;

			case ClockInput.SET_ALARM:
				prevValue = ClockInput.SET_ALARM;

				break;

			case ClockInput.SHOW_TIME:
				if(prevValue == ClockInput.SET_ALARM){
					int t = i.getValue();
					int hour = t / 10000;
					int min = (t/100) - hour*100;
					int sec = t - (hour*10000) - (min * 100);
					sd.setAlarm(new Time(hour, min, sec));
					//ALARM
				}
				else if(prevValue == ClockInput.SET_TIME){
					// Time
					int t = i.getValue();
					int hour = t / 10000;
					int min = (t/100) - hour*100;
					int sec = t - (hour*10000) - (min * 100);
					sd.setTime(new Time(hour, min, sec));
				}
				
				
				prevValue = ClockInput.SHOW_TIME;

			}

		}
	}

}
