package todo;

import java.time.Clock;

import done.*;
import se.lth.cs.realtime.semaphore.MutexSem;
import se.lth.cs.realtime.semaphore.Semaphore;

public class SharedData {
	private MutexSem mutex;
	private Time currentTime;
	private Time alarmTime;
	private ClockOutput output;
	private Semaphore sem;
	private ClockInput i;
	private int alarmCount;
	private boolean alarm;

	public SharedData(ClockOutput o, ClockInput i) {
		currentTime = new Time();
		alarmTime = new Time();
		mutex = new MutexSem();
		output = o;
		this.i = i;
		alarmCount = 20;
		alarm = false;
	}

	/* Sets the time * */
	public void setTime(Time time) {
		mutex.take();
		currentTime = time;
		output.showTime(currentTime.getTime());
		mutex.give();
	}

	/* Sets the alarm-time */
	public void setAlarm(Time tiem) {
		mutex.take();
		alarmTime = tiem;
		mutex.give();
	}

	/* Increases the current time by one second */
	public void increaseTime() {
		mutex.take();
		currentTime.increase();
		output.showTime(currentTime.getTime());
		System.out.println("CurrentTime: " + currentTime + " AlarmTime: "
				+ alarmTime + " Alarmflag: " + i.getAlarmFlag());
		if (currentTime.equals(alarmTime) && i.getAlarmFlag()) {
			alarm = true;
		}
		if (alarm && alarmCount >= 0) {
			output.doAlarm();
			alarmCount--;
		}
		if (alarmCount == 0) {
			alarm = false;
		}
		mutex.give();
	}
}
