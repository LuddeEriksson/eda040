package todo;

public class Time {
	private int hour;
	private int minute;
	private int second;
	private final int MAX_HOUR = 23;
	private final int MAX_MINUTE = 59;
	private final int MAX_SECOND = 59;

	public Time() {
		hour = 0;
		minute = 0;
		second = 0;

	}

	public Time(int hour, int minute, int second) {
		this.hour = hour;
		this.minute = minute;
		this.second = second;
	}

	public void increase() {
		if (second < MAX_SECOND) {
			second++;
		} else {
			second = 0;
			if (minute < MAX_MINUTE) {
				minute++;
			} else {
				minute = 0;
				if (hour < MAX_HOUR) {
					hour++;
				} else {
					hour = 0;
				}
			}
		}
	}

	// returns the time in the same format as output requires (HHMMSS)
	public int getTime() {
		return hour * 10000 + minute * 100 + second;
	}
	public String toString(){
		return Integer.toString((hour * 10000 + minute * 100 + second));
	}

	public boolean equals(Object obj) {
		if (obj instanceof Time) {
			Time tm = (Time) obj;
			return tm.hour == hour && tm.minute == minute
					&& tm.second == second;
		}
		return false;
	}

}
