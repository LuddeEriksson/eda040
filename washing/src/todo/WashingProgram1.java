/*
 * Real-time and concurrent programming course, laboratory 3
 * Department of Computer Science, Lund Institute of Technology
 *
 * PP 980812 Created
 * PP 990924 Revised
 */

package todo;

import done.*;

/**
 * Program ` of washing machine. Does the following:
 * <UL>
 * <LI>Switches off heating
 * <LI>Switches off spi1
 * <LI>Pumps out water
 * <LI>Unlocks the hatch.
 * </UL>
 */
class WashingProgram1 extends WashingProgram {

	// ------------------------------------------------------------- CONSTRUCTOR

	/**
	 * @param mach
	 *            The washing machine to control
	 * @param speed
	 *            Simulation speed
	 * @param tempController
	 *            The TemperatureController to use
	 * @param waterController
	 *            The WaterController to use
	 * @param spinController
	 *            The SpinController to use
	 */
	public WashingProgram1(AbstractWashingMachine mach, double speed,
			TemperatureController tempController,
			WaterController waterController, SpinController spinController) {
		super(mach, speed, tempController, waterController, spinController);
	}

	// ---------------------------------------------------------- PUBLIC METHODS

	/**
	 * This method contains the actual code for the washing program. Executed
	 * when the start() method is called.
	 */
	protected void wash() throws InterruptedException {

		// lock the hatch
		myMachine.setLock(true);

		// Fill with water
		myWaterController.putEvent(new WaterEvent(this, WaterEvent.WATER_FILL,
				0.50));
		mailbox.doFetch(); // wait for ack

		mySpinController.putEvent(new SpinEvent(this, SpinEvent.SPIN_SLOW));

		// heat
		myTempController.putEvent(new TemperatureEvent(this,
				TemperatureEvent.TEMP_SET, 60));
		mailbox.doFetch(); // wait for ack
		System.out.println("sleep");
		Thread.sleep((long) (1800000 / mySpeed));
		System.out.println("Sleeping done");
		// executes the temp below before sleeping, not sure why
		myTempController.putEvent(new TemperatureEvent(this,
				TemperatureEvent.TEMP_IDLE, 0));
		System.out.println("Heating turned off");
		// drain
		mySpinController.putEvent(new SpinEvent(this, SpinEvent.SPIN_OFF));
		myWaterController.putEvent(new WaterEvent(this, WaterEvent.WATER_DRAIN,
				0));
		mailbox.doFetch();
		System.out.println("Rinse starts");
		// rinse x5
		for (int i = 0; i < 5; i++) {
			myWaterController.putEvent(new WaterEvent(this,
					WaterEvent.WATER_FILL, 0.6));
			mailbox.doFetch();
			mySpinController.putEvent(new SpinEvent(this, SpinEvent.SPIN_SLOW));
			Thread.sleep((long) (120000 / mySpeed));
			mySpinController.putEvent(new SpinEvent(this, SpinEvent.SPIN_OFF));
			myWaterController.putEvent(new WaterEvent(this,
					WaterEvent.WATER_DRAIN, 0));
			mailbox.doFetch();
		}
		// time to centrifuge
		mySpinController.putEvent(new SpinEvent(this, SpinEvent.SPIN_FAST));
		System.out.println("Centrifuging");
		Thread.sleep((long) (300000 / mySpeed));
		mySpinController.putEvent(new SpinEvent(this, SpinEvent.SPIN_OFF));
		System.out.println("unlocking");
		
		if (myMachine.getWaterLevel() != 0) {
			myWaterController.putEvent(new WaterEvent(this,
					WaterEvent.WATER_DRAIN, 0));
			mailbox.doFetch();
		}
		myMachine.setLock(false);

	}
}
