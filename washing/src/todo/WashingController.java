package todo;

import done.*;

public class WashingController implements ButtonListener {
	private AbstractWashingMachine machine;
	private double speed;
	private SpinController sc;
	private TemperatureController tc;
	private WaterController wc;
	private WashingProgram w;
	private boolean running;

	public WashingController(AbstractWashingMachine theMachine, double theSpeed) {
		machine = theMachine;
		speed = theSpeed;
		sc = new SpinController(machine, speed);
		tc = new TemperatureController(machine, speed);
		wc = new WaterController(machine, speed);
		sc.start();
		tc.start();
		wc.start();
		running = false;
	}

	public void processButton(int theButton) {
		if (theButton == 0) {
			if (w != null) {
				w.interrupt();
				running = false;
			}
			else{
				System.err.println("No program running");
			}
		}
			else if (!running || !w.isAlive()) {

			switch (theButton) {
			case 1:
				w = new WashingProgram1(machine, speed, tc, wc, sc);
				break;
			case 2:
				w = new WashingProgram2(machine, speed, tc, wc, sc);
				break;
			case 3:
				w = new WashingProgram3(machine, speed, tc, wc, sc);
				break;
			}
			w.start();
			running = true;

			}
		}
	}
