package todo;

import se.lth.cs.realtime.*;
import done.AbstractWashingMachine;

public class WaterController extends PeriodicThread {
	// TODO: add suitable attributes
	private AbstractWashingMachine machine;
	private boolean finished;
	private WaterEvent currentEvent;
	private int mode;
	private double level;

	public WaterController(AbstractWashingMachine mach, double speed) {
		super((long) (500.0 / speed)); // TODO: replace with suitable period
		machine = mach;
		finished = true;
	}

	public void perform() {

		WaterEvent event = (WaterEvent) mailbox.tryFetch();
		// will only run once as mailbox will be empty otherwise
		if (event != null) {
			if (!finished) {
				// interrupted, fix it
			}
			finished = false;
			currentEvent = event;

			mode = event.getMode();
			level = event.getLevel();
			switch (mode) {

			case WaterEvent.WATER_FILL:
				machine.setDrain(false);
				machine.setFill(true);
				break;
			case WaterEvent.WATER_DRAIN:
				machine.setDrain(true);
				machine.setFill(false);
				break;
			case WaterEvent.WATER_IDLE:
				machine.setDrain(false);
				machine.setFill(false);
			}

		}
		// runs every time perform is called
		if (mode == WaterEvent.WATER_DRAIN && machine.getWaterLevel() <= 0) {
			machine.setFill(false);
			machine.setDrain(false);
			if(!finished){
				
			((RTThread) currentEvent.getSource()).putEvent(new AckEvent(this));
			finished = true;
			}
		}
		else if(mode == WaterEvent.WATER_FILL && machine.getWaterLevel() >= level){
			machine.setFill(false);
			if(!finished){
				
			((RTThread) currentEvent.getSource()).putEvent(new AckEvent(this));
			finished = true;
			}

		}
	}
}