package todo;

import se.lth.cs.realtime.*;
import done.AbstractWashingMachine;

public class SpinController extends PeriodicThread {
	private AbstractWashingMachine machine;
	private int mode;
	private int direction;

	public SpinController(AbstractWashingMachine mach, double speed) {
		super((long) (60000 / speed)); 
		machine = mach;
	}

	public void perform() {
		SpinEvent event = ((SpinEvent) mailbox.tryFetch());
		if (event != null) {
			mode = event.getMode();
			switch (mode) {
			case SpinEvent.SPIN_FAST:
				if(machine.getWaterLevel() > 0) {
					System.err.println("Still water in the machine when trying to centrifuge");
				} else {
					machine.setSpin(AbstractWashingMachine.SPIN_FAST);					
				}
				break;
			case SpinEvent.SPIN_SLOW:
				machine.setSpin(AbstractWashingMachine.SPIN_LEFT);
				direction = -1;
				break;
			case SpinEvent.SPIN_OFF:
				machine.setSpin(AbstractWashingMachine.SPIN_OFF);
				break;
			}
		}
		//runs every time
		if(mode == SpinEvent.SPIN_SLOW) {
			switch (direction) {
			case -1:
				machine.setSpin(AbstractWashingMachine.SPIN_RIGHT);
				direction = 1;
				break;
			case 1:
				machine.setSpin(AbstractWashingMachine.SPIN_LEFT);
				direction = -1;
				break;
			}
		}
	}
}