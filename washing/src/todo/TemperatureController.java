package todo;

import se.lth.cs.realtime.*;
import done.AbstractWashingMachine;

public class TemperatureController extends PeriodicThread {
	// TODO: add suitable attributes
	private AbstractWashingMachine machine;
	private double speed;
	private TemperatureEvent currentEvent;
	private int mode;
	private double target;
	private boolean finished;

	public TemperatureController(AbstractWashingMachine mach, double speed) {
		super((long) (1000.0 / speed)); // TODO: replace with suitable period
		machine = mach;
		this.speed = speed;
		finished = true;
		currentEvent = new TemperatureEvent(null, TemperatureEvent.TEMP_IDLE, 0);
	}

	public void perform() {
		TemperatureEvent event = (TemperatureEvent) mailbox.tryFetch();
		// run once
		if (event != null) {
			finished = false;
			currentEvent = event;
			mode = event.getMode();
			target = event.getTemperature();
			if (mode == TemperatureEvent.TEMP_IDLE) {
				machine.setHeating(false);
				((RTThread) currentEvent.getSource()).putEvent(new AckEvent(
						this));

			}
		}
		// run all the time
		if (currentEvent.getMode() == TemperatureEvent.TEMP_SET) {
			if (machine.getTemperature() < target - 2
					&& machine.getWaterLevel() != 0) {
				machine.setHeating(true);
			} else if (machine.getTemperature() >= target - 0.2) {
				machine.setHeating(false);
				// need to send an ack first time it completes
				if (!finished) {
					System.out.println("finished heating");
					finished = true;
					((RTThread) currentEvent.getSource())
							.putEvent(new AckEvent(this));
				}
			}
		}

	}
}
